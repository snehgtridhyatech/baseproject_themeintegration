# BaseProject ThemeIntegration

- The base project includes set up for single or multiple themes. You can manage multiple themes and see live color changes to all components using the base project. The necessary code has been completed in this base setup. You only need to utillize those functions in your project.


## Result

![](demo.gif){:height="400px" width="200px"}


## Getting started

1. Clone the project from here:

```
git clone https://gitlab.com/snehgtridhyatech/baseproject_themeintegration.git
```

2. Copy **ThemeManager** file from this project into your project.


## Usage

**1. Create themes array.**
 
```
var themes = [ThemeModel]()
```

**2. Add your themes in to your themes array.**

```    
themes.append(ThemeModel(index: 0,
                         labelColor: CodableColor(color: .white),
                         viewControllerBackgroundColor: CodableColor(color: .black),
                         buttonTextColor: CodableColor(color: .black),
                         buttonBackgroundColor: CodableColor(color: .white),
                         switchOnColor: CodableColor(color: .white),
                         switchThumbColor: CodableColor(color: .gray),
                         switchOffColor: CodableColor(color: .systemGray5),
                         sliderFilledTrackColor: CodableColor(color: .white),
                         sliderUnFilledTrackColor: CodableColor(color: .gray),
                         sliderThumbColor: CodableColor(color: .white),
                         segmentSelectedBackgroundColor: CodableColor(color: .black)))
           
themes.append(ThemeModel(index: 1,
                         labelColor: CodableColor(color: .black),
                         viewControllerBackgroundColor: CodableColor(color: .white),
                         buttonTextColor: CodableColor(color: .white),
                         buttonBackgroundColor: CodableColor(color: .black),
                         switchOnColor: CodableColor(color: .green),
                         switchThumbColor: CodableColor(color: .white),
                         switchOffColor: CodableColor(color: .gray),
                         sliderFilledTrackColor: CodableColor(color: .green),
                         sliderUnFilledTrackColor: CodableColor(color: .gray),
                         sliderThumbColor: CodableColor(color: .green),
                         segmentSelectedBackgroundColor: CodableColor(color: .white)))

```

**3. Set selected theme. Set your index at {SELECTED_INDEX}.**
 
```
APP_CURRENT_THEME = self.themes[{SELECTED_INDEX}]

ThemeManager.shared.setAppearence(controller: self)
```

**4. Save selected theme. Set your User default key at {USERDEFAULT_KEY}.**
 
```
ThemeManager.shared.saveTheme(key: {USERDEFAULT_KEY})
```
        
        
**5. Retrive theme and change color. Set your User default key at {USERDEFAULT_KEY}.**
 
```
if let savedTheme =  ThemeManager.shared.getSavedTheme(key: {USERDEFAULT_KEY}) {
 
    APP_CURRENT_THEME = savedTheme
 
    ThemeManager.shared.setAppearence(controller: self)
}
```


## Customization

**You can customize all components as per your requirement.**
 
```
/// Custom theme model (Use null values to assign system color)
struct ThemeModel : Codable {
    
    /// Index of theme
    var index : Int
    
    /// Change color of all labels
    var labelColor : CodableColor? = nil
    
    /// Change background color of all view controllers
    var viewControllerBackgroundColor : CodableColor? = nil
    
    /// Change  color of all button text
    var buttonTextColor : CodableColor? = nil
    
    /// Change background color of all button
    var buttonBackgroundColor : CodableColor? = nil
    
    /// Change border color of all button
    var buttonBorderColor : CodableColor? = nil
    
    /// Change background color of all textfield
    var textfieldBackgroundColor : CodableColor? = nil
    
    /// Change text color of all textfield
    var textfieldTextColor : CodableColor? = nil
    
    /// Change placeholder color of all textfield
    var textfieldPlaceholderColor : CodableColor? = nil
    
    /// Change border color of all textfield
    var textfieldBorderColor : CodableColor? = nil
    
    /// Change background color of all textviews
    var textViewBackgroundColor : CodableColor? = nil
    
    /// Change text color of all textviews
    var textViewTextColor : CodableColor? = nil
    
    /// Change placeholder color of all textviews
    var textViewPlaceholderColor : CodableColor? = nil
    
    /// Change border color of all textviews
    var textViewBorderColor : CodableColor? = nil
    
    /// Change background color of navigation bar
    var navBarBackgroundColor : CodableColor? = nil
    
    /// Change text color of navigation bar
    var navBarTextColor : CodableColor? = nil
    
    /// Change tint color of navigation bar
    var navBarTintColor : CodableColor? = nil
    
    /// Change item color of navigation bar
    var navBarItemColor : CodableColor? = nil
    
    /// Change item color of tab bar
    var tabbarItemColor : CodableColor? = nil
    
    /// Change background color of tab bar
    var tabbarBackgroundColor : CodableColor? = nil
    
    /// Change unselected item color of tab bar
    var tabbarUnselectedItemColor : CodableColor? = nil
    
    /// Change tint color of tab bar
    var tabbarTintColor : CodableColor? = nil
    
    /// Change switch color in on condition
    var switchOnColor : CodableColor? = nil
    
    /// Change thumb color of switch
    var switchThumbColor : CodableColor? = nil
    
    /// Change switch color in off condition
    var switchOffColor : CodableColor? = nil
    
    /// Change filled color of slider
    var sliderFilledTrackColor : CodableColor? = nil
    
    /// Change unfilled color of slider
    var sliderUnFilledTrackColor : CodableColor? = nil
    
    /// Change thumb color of slider
    var sliderThumbColor : CodableColor? = nil
    
    /// Change background color of selected segement item
    var segmentSelectedBackgroundColor : CodableColor? = nil
    
    /// Change background color of segement
    var segmentBackgroundColor : CodableColor? = nil
    
    /// Change filled color of progressbar
    var progressFilledColor : CodableColor? = nil
    
    /// Change unfilled color of progressbar
    var progressUnFilledColor : CodableColor? = nil
    
    /// Change color of activity indicator
    var activityIndicatorColor : CodableColor? = nil
}
```
