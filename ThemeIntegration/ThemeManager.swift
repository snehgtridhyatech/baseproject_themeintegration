//
//  ThemeManager.swift
//  ThemeIntegration
//
//  Created by Sneh on 10/12/23.
//

import Foundation
import UIKit

/// Current selected theme
var APP_CURRENT_THEME = ThemeModel(index: 0)


/// Custom theme appearence
class ThemeManager {
    
    // MARK: - Properties
    
    /// Shared instance of  class
    static let shared = ThemeManager()
    
    
    // MARK: - Functions
    
    /// Use this function to change theme appearence of your app. Set APP_CURRENT_THEME before using this function.
    func setAppearence(controller: UIViewController) {
        self.customizeControllers(controller: controller)
        self.customizeLabels()
        self.customizeTextViews()
        self.customizeTextFields()
        self.customizeButtons()
        self.customizeNavigationBar()
        self.customizeTabBar()
        self.customizeSwitches()
        self.customizeSliders()
        self.customizeSegmentControls()
        self.customizeProgressView()
        self.customizeIndicatorView()
        self.reloadAllViews()
    }
    
    
    // MARK: - Controller theme properties
    fileprivate func customizeControllers(controller: UIViewController) {
        DispatchQueue.main.async {
            controller.view.backgroundColor = (APP_CURRENT_THEME.viewControllerBackgroundColor != nil) ? APP_CURRENT_THEME.viewControllerBackgroundColor?.uiColor : .systemBackground
        }
    }
    
    
    // MARK: - Label theme properties
    fileprivate func customizeLabels() {
        UILabel.appearance().textColor = (APP_CURRENT_THEME.labelColor != nil) ? APP_CURRENT_THEME.labelColor?.uiColor : .label
    }
    
    
    // MARK: - TextView theme properties
    fileprivate func customizeTextViews() {
        let textView = UITextView()
        UITextView.appearance().textColor = (APP_CURRENT_THEME.textViewTextColor != nil) ? APP_CURRENT_THEME.textViewTextColor?.uiColor : textView.textColor
        UITextView.appearance().backgroundColor = (APP_CURRENT_THEME.textViewBackgroundColor != nil) ? APP_CURRENT_THEME.textViewBackgroundColor?.uiColor : textView.backgroundColor
        UITextView.appearance().layer.borderColor = (APP_CURRENT_THEME.textViewBorderColor != nil) ? APP_CURRENT_THEME.textViewBorderColor?.uiColor.cgColor : textView.layer.borderColor
    }
    
    
    // MARK: - TextField theme properties
    fileprivate func customizeTextFields() {
        let textField = UITextField()
        UITextField.appearance().backgroundColor = (APP_CURRENT_THEME.textViewBackgroundColor != nil) ? APP_CURRENT_THEME.textfieldBackgroundColor?.uiColor : textField.backgroundColor
        UITextField.appearance().textColor = (APP_CURRENT_THEME.textfieldTextColor != nil) ? APP_CURRENT_THEME.textfieldTextColor?.uiColor : textField.textColor
        UITextField.appearance().layer.borderColor = (APP_CURRENT_THEME.textfieldBorderColor != nil) ? APP_CURRENT_THEME.textfieldBorderColor?.uiColor.cgColor : textField.layer.borderColor
    }
    
    
    // MARK: - Button theme properties
    fileprivate func customizeButtons() {
        let button = UIButton()
        UIButton.appearance().setTitleColor((APP_CURRENT_THEME.buttonTextColor != nil) ? APP_CURRENT_THEME.buttonTextColor?.uiColor : .label, for: .normal)
        UIButton.appearance().layer.borderColor = (APP_CURRENT_THEME.buttonBorderColor != nil) ? APP_CURRENT_THEME.buttonBorderColor?.uiColor.cgColor : button.layer.borderColor
        UIButton.appearance().backgroundColor = (APP_CURRENT_THEME.buttonBackgroundColor != nil) ? APP_CURRENT_THEME.buttonBackgroundColor?.uiColor : button.backgroundColor
    }
    
    
    // MARK: - NavigationBar theme properties
    fileprivate func customizeNavigationBar() {
        let navigationBar = UINavigationBar()
        let barButtonItem = UIBarButtonItem()
        UINavigationBar.appearance().barTintColor = (APP_CURRENT_THEME.navBarBackgroundColor != nil) ? APP_CURRENT_THEME.navBarBackgroundColor?.uiColor : navigationBar.barTintColor
        UINavigationBar.appearance().tintColor = (APP_CURRENT_THEME.navBarTintColor != nil) ? APP_CURRENT_THEME.navBarTintColor?.uiColor : navigationBar.tintColor
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : (APP_CURRENT_THEME.navBarTintColor != nil) ? APP_CURRENT_THEME.navBarTintColor?.uiColor ?? .tintColor : UIColor.tintColor]
        UIBarButtonItem.appearance().tintColor = (APP_CURRENT_THEME.navBarItemColor != nil) ? APP_CURRENT_THEME.navBarItemColor?.uiColor : barButtonItem.tintColor
    }
    
    
    // MARK: - TabBar theme properties
    fileprivate func customizeTabBar() {
        let tabBar = UITabBar()
        UITabBar.appearance().barTintColor = (APP_CURRENT_THEME.tabbarTintColor != nil) ? APP_CURRENT_THEME.tabbarTintColor?.uiColor : tabBar.barTintColor
        UITabBar.appearance().tintColor = (APP_CURRENT_THEME.tabbarItemColor != nil) ? APP_CURRENT_THEME.tabbarItemColor?.uiColor : tabBar.tintColor
        UITabBar.appearance().unselectedItemTintColor = (APP_CURRENT_THEME.tabbarUnselectedItemColor != nil) ? APP_CURRENT_THEME.tabbarUnselectedItemColor?.uiColor : tabBar.tintColor
        UITabBar.appearance().backgroundColor = (APP_CURRENT_THEME.tabbarBackgroundColor != nil) ? APP_CURRENT_THEME.tabbarBackgroundColor?.uiColor : tabBar.tintColor
    }
    
    
    // MARK: - Switch theme properties
    fileprivate func customizeSwitches() {
        let uiSwitch = UISwitch()
        UISwitch.appearance().onTintColor = (APP_CURRENT_THEME.switchOnColor != nil) ? APP_CURRENT_THEME.switchOnColor?.uiColor : uiSwitch.onTintColor
        UISwitch.appearance().thumbTintColor = (APP_CURRENT_THEME.switchThumbColor != nil) ? APP_CURRENT_THEME.switchThumbColor?.uiColor : uiSwitch.thumbTintColor
        UISwitch.appearance().tintColor = (APP_CURRENT_THEME.switchOffColor != nil) ? APP_CURRENT_THEME.switchOffColor?.uiColor : uiSwitch.tintColor
    }
    
    
    // MARK: - Slider theme properties
    fileprivate func customizeSliders() {
        let slider = UISlider()
        UISlider.appearance().maximumTrackTintColor = (APP_CURRENT_THEME.sliderUnFilledTrackColor != nil) ? APP_CURRENT_THEME.sliderUnFilledTrackColor?.uiColor : slider.minimumTrackTintColor
        UISlider.appearance().minimumTrackTintColor = (APP_CURRENT_THEME.sliderFilledTrackColor != nil) ? APP_CURRENT_THEME.sliderFilledTrackColor?.uiColor : slider.maximumTrackTintColor
        UISlider.appearance().thumbTintColor = (APP_CURRENT_THEME.sliderThumbColor != nil) ? APP_CURRENT_THEME.sliderThumbColor?.uiColor : slider.thumbTintColor
    }
    
    
    // MARK: - Segement theme properties
    fileprivate func customizeSegmentControls() {
        let segmentControl = UISegmentedControl()
        UISegmentedControl.appearance().selectedSegmentTintColor = (APP_CURRENT_THEME.segmentSelectedBackgroundColor != nil) ? APP_CURRENT_THEME.segmentSelectedBackgroundColor?.uiColor : segmentControl.selectedSegmentTintColor
        UISegmentedControl.appearance().backgroundColor = (APP_CURRENT_THEME.segmentBackgroundColor != nil) ? APP_CURRENT_THEME.segmentBackgroundColor?.uiColor : segmentControl.backgroundColor
    }
    
    
    // MARK: - ProgressView theme properties
    fileprivate func customizeProgressView() {
        let progressView = UIProgressView()
        UIProgressView.appearance().progressTintColor = (APP_CURRENT_THEME.progressFilledColor != nil) ? APP_CURRENT_THEME.progressFilledColor?.uiColor : progressView.progressTintColor
        UIProgressView.appearance().trackTintColor = (APP_CURRENT_THEME.progressUnFilledColor != nil) ?  APP_CURRENT_THEME.progressUnFilledColor?.uiColor : progressView.trackTintColor
    }
    
    
    // MARK: - ActivityIndicator theme properties
    fileprivate func customizeIndicatorView() {
        let indicatorView = UIActivityIndicatorView()
        UIActivityIndicatorView.appearance().color = (APP_CURRENT_THEME.activityIndicatorColor != nil) ? APP_CURRENT_THEME.activityIndicatorColor?.uiColor : indicatorView.color
    }
    
    
    // MARK: - Reloading views
    fileprivate func reloadAllViews() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate, let window = appDelegate.window {
            window.subviews.forEach { view in
                DispatchQueue.main.async {
                    view.removeFromSuperview()
                    window.addSubview(view)
                }
            }
        }
    }
    
    
    // MARK: - Userdefault functions
    
    /// Save selected theme (APP_CURRENT_THEME) to userdefault
    func saveTheme(key: String) {
        do {
            let data = try JSONEncoder().encode(APP_CURRENT_THEME)
            UserDefaults.standard.set(data, forKey: key)
        } catch (let err) {
            print("Error in saveTheme: \(err.localizedDescription)")
        }
    }
    
    /// Retrive theme from userdefault
    func getSavedTheme(key: String) -> ThemeModel? {
        do {
            if let data = UserDefaults.standard.object(forKey: key) {
                let theme = try JSONDecoder().decode(ThemeModel.self, from: data as! Data)
                return theme
            } else {
                return nil
            }
        } catch (let err) {
            print("Error in getSavedTheme: \(err.localizedDescription)")
            return nil
        }
    }
}


// MARK: - Model class

/// Custom theme model (Use null values to assign system color)
struct ThemeModel : Codable {
    
    /// Index of theme
    var index : Int
    
    /// Change color of all labels
    var labelColor : CodableColor? = nil
    
    /// Change background color of all view controllers
    var viewControllerBackgroundColor : CodableColor? = nil
    
    /// Change  color of all button text
    var buttonTextColor : CodableColor? = nil
    
    /// Change background color of all button
    var buttonBackgroundColor : CodableColor? = nil
    
    /// Change border color of all button
    var buttonBorderColor : CodableColor? = nil
    
    /// Change background color of all textfield
    var textfieldBackgroundColor : CodableColor? = nil
    
    /// Change text color of all textfield
    var textfieldTextColor : CodableColor? = nil
    
    /// Change placeholder color of all textfield
    var textfieldPlaceholderColor : CodableColor? = nil
    
    /// Change border color of all textfield
    var textfieldBorderColor : CodableColor? = nil
    
    /// Change background color of all textviews
    var textViewBackgroundColor : CodableColor? = nil
    
    /// Change text color of all textviews
    var textViewTextColor : CodableColor? = nil
    
    /// Change placeholder color of all textviews
    var textViewPlaceholderColor : CodableColor? = nil
    
    /// Change border color of all textviews
    var textViewBorderColor : CodableColor? = nil
    
    /// Change background color of navigation bar
    var navBarBackgroundColor : CodableColor? = nil
    
    /// Change text color of navigation bar
    var navBarTextColor : CodableColor? = nil
    
    /// Change tint color of navigation bar
    var navBarTintColor : CodableColor? = nil
    
    /// Change item color of navigation bar
    var navBarItemColor : CodableColor? = nil
    
    /// Change item color of tab bar
    var tabbarItemColor : CodableColor? = nil
    
    /// Change background color of tab bar
    var tabbarBackgroundColor : CodableColor? = nil
    
    /// Change unselected item color of tab bar
    var tabbarUnselectedItemColor : CodableColor? = nil
    
    /// Change tint color of tab bar
    var tabbarTintColor : CodableColor? = nil
    
    /// Change switch color in on condition
    var switchOnColor : CodableColor? = nil
    
    /// Change thumb color of switch
    var switchThumbColor : CodableColor? = nil
    
    /// Change switch color in off condition
    var switchOffColor : CodableColor? = nil
    
    /// Change filled color of slider
    var sliderFilledTrackColor : CodableColor? = nil
    
    /// Change unfilled color of slider
    var sliderUnFilledTrackColor : CodableColor? = nil
    
    /// Change thumb color of slider
    var sliderThumbColor : CodableColor? = nil
    
    /// Change background color of selected segement item
    var segmentSelectedBackgroundColor : CodableColor? = nil
    
    /// Change background color of segement
    var segmentBackgroundColor : CodableColor? = nil
    
    /// Change filled color of progressbar
    var progressFilledColor : CodableColor? = nil
    
    /// Change unfilled color of progressbar
    var progressUnFilledColor : CodableColor? = nil
    
    /// Change color of activity indicator
    var activityIndicatorColor : CodableColor? = nil
}



/// Codable color class to save theme color in Userdefaults
struct CodableColor: Codable {
    let red: CGFloat
    let green: CGFloat
    let blue: CGFloat
    let alpha: CGFloat
    
    init(color: UIColor) {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        
        color.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        self.red = red
        self.green = green
        self.blue = blue
        self.alpha = alpha
    }
    
    var uiColor: UIColor {
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}
