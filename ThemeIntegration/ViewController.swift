//
//  ViewController.swift
//  ThemeIntegration
//
//  Created by Sneh on 10/12/23.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var segment: UISegmentedControl!
    
    var themes = [ThemeModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.themes.append(ThemeModel(index: 0))
        self.themes.append(ThemeModel(index: 1,
                                      labelColor: CodableColor(color: .white),
                                      viewControllerBackgroundColor: CodableColor(color: .black),
                                      buttonTextColor: CodableColor(color: .black),
                                      buttonBackgroundColor: CodableColor(color: .white),
                                      switchOnColor: CodableColor(color: .white),
                                      switchThumbColor: CodableColor(color: .gray),
                                      switchOffColor: CodableColor(color: .systemGray5),
                                      sliderFilledTrackColor: CodableColor(color: .white),
                                      sliderUnFilledTrackColor: CodableColor(color: .gray),
                                      sliderThumbColor: CodableColor(color: .white),
                                      segmentSelectedBackgroundColor: CodableColor(color: .black)))
        self.themes.append(ThemeModel(index: 2,
                                      labelColor: CodableColor(color: .black),
                                      viewControllerBackgroundColor: CodableColor(color: .white),
                                      buttonTextColor: CodableColor(color: .white),
                                      buttonBackgroundColor: CodableColor(color: .black),
                                      switchOnColor: CodableColor(color: .green),
                                      switchThumbColor: CodableColor(color: .white),
                                      switchOffColor: CodableColor(color: .gray),
                                      sliderFilledTrackColor: CodableColor(color: .green),
                                      sliderUnFilledTrackColor: CodableColor(color: .gray),
                                      sliderThumbColor: CodableColor(color: .green),
                                      segmentSelectedBackgroundColor: CodableColor(color: .white)))
        self.themes.append(ThemeModel(index: 3,
                                      labelColor: CodableColor(color: .red),
                                      viewControllerBackgroundColor: CodableColor(color: .yellow),
                                      buttonTextColor: CodableColor(color: .white),
                                      buttonBackgroundColor: CodableColor(color: .red),
                                      switchOnColor: CodableColor(color: .red),
                                      switchThumbColor: CodableColor(color: .yellow),
                                      switchOffColor: CodableColor(color: .systemGray5),
                                      sliderFilledTrackColor: CodableColor(color: .red),
                                      sliderUnFilledTrackColor: CodableColor(color: .gray),
                                      sliderThumbColor: CodableColor(color: .red),
                                      segmentSelectedBackgroundColor: CodableColor(color: .yellow),
                                      segmentBackgroundColor: CodableColor(color: .yellow)))
        
        if let savedTheme =  ThemeManager.shared.getSavedTheme(key: "theme") {
            self.segment.selectedSegmentIndex = savedTheme.index
            APP_CURRENT_THEME = savedTheme
            ThemeManager.shared.setAppearence(controller: self)
        }

    }
    
    
    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        APP_CURRENT_THEME = self.themes[sender.selectedSegmentIndex]
        ThemeManager.shared.setAppearence(controller: self)
        ThemeManager.shared.saveTheme(key: "theme")
    }
}
